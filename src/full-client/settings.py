# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ui_dialogsettings import Ui_DialogSettings

class DialogSettings(QDialog, Ui_DialogSettings):
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)
        settings = QSettings()
        self.lineIdentNick.setText(settings.value("ident/nick").toString())
        self.lineIdentPass.setText(settings.value("ident/password").toString())
        self.lineRemoteAddr.setText(settings.value("network/remote_server").toString())
        self.lineRemotePort.setText(settings.value("network/remote_port").toString())
        self.lineLocalAddr.setText(settings.value("network/local_server").toString())
        self.lineLocalPort.setText(settings.value("network/local_port").toString())

    def accept(self):
        settings = QSettings()
        settings.setValue("ident/nick", self.lineIdentNick.text())
        settings.setValue("ident/password", self.lineIdentPass.text())
        settings.setValue("network/remote_server", self.lineRemoteAddr.text())
        settings.setValue("network/remote_port", self.lineRemotePort.text())
        settings.setValue("network/local_server", self.lineLocalAddr.text())
        settings.setValue("network/local_port", self.lineLocalPort.text())
        self.done(1)

    def reject(self):
        self.done(0)
