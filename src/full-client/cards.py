# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

import random, re
from PyQt4.QtGui import QPixmap, QLabel
from PyQt4 import QtCore

def makeRandomDeck():
    """
    Build a complete deck (52 cards)
    """
    cards = [ "C00", "C01", "C02", "C03", "C04", "C05", "C06", "C07", "C08", "C09", "C10", \
         "C11", "C12", "Q00", "Q01", "Q02", "Q03", "Q04", "Q05", "Q06", "Q07", "Q08", \
         "Q09", "Q10", "Q11", "Q12", "F00", "F01", "F02", "F03", "F04", "F05", "F06", \
         "F07", "F08", "F09", "F10", "F11", "F12", "P00", "P01", "P02", "P03", "P04", \
         "P05", "P06", "P07", "P08", "P09", "P10", "P11", "P12" ]
    random.shuffle(cards)
    return cards

def checkCardFormat(card):
    q = re.compile("[C|P|Q|F][0-9]+")
    if q.match(card):
        return True
    else:
        return False

class Game(QtCore.QObject):
    """
    Represent the logic behind the hearts game
    """

    brokenHeartsSign = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        QtCore.QObject.__init__(self, parent)
        self.handFinished = None
        self.nextPlayer = None
        self.brokenHearts = False
        self.hands = []
        self.currentHand = None      # pointer into self.hands to the current hand
        self.totalPoints = []             # Key: integer for player, Value: integer for points
        self.gamePoints = []

        self.currentHand = Hand(self.brokenHearts)
        self.hands.append(self.currentHand)

        for i in range(0, 4):
            self.totalPoints.append(0)
            self.gamePoints.append(0)

    def resetGame(self):
        self.handFinished = None
        self.nextPlayer = None
        self.brokenHearts = False
        self.hands = []
        self.currentHand = None      # pointer into self.hands to the current hand
        self.currentHand = Hand(self.brokenHearts)
        self.hands.append(self.currentHand)
        self.gamePoints = []
        for i in range(0, 4):
            self.gamePoints.append(0)

    def cardPlayed(self, playerNr, playerDeck, card):
        """
        The player playerNr with deck = playerDeck has played a card.
        Must control if he can do this.
        
        returns (boolean, reason)
        """
        assert(isinstance(playerNr, int))
        assert(isinstance(playerDeck, list))
        assert(isinstance(card, str))
        assert(checkCardFormat(card))

        # First card must be 2 Fiori
        if len(self.hands) == 1 and len(self.hands[0].players) == 0 and not card == "F00":
            return (False, "First card must be 2 Fiori")

        # No penalty cards on first hand
        if len(self.hands) == 1:
            c = re.compile("C")
            q = re.compile("P10")
            if q.match(card) or c.match(card):
                return (False, "No penalty card on first hand")

        if card not in playerDeck:
            return (False, "The client doesn't have such card")

        res, error = self.currentHand.cardPlayed(playerNr, playerDeck, card)

        if res:
            # Are the hearts broken?
            heartsInCard = re.compile("C")
            if heartsInCard.match(card) and not self.brokenHearts:
                self.brokenHearts = True
                self.brokenHeartsSign.emit()

            if len(self.currentHand.players) >= 4:
                # Next hand.. needs to calc points and who win the hand,
                # plus initialize a new hand object
                self.nextPlayer, point = self.currentHand.calcPoint()
                self.gamePoints[self.nextPlayer] += int(point)
                self.currentHand = Hand(self.brokenHearts)
                self.hands.append(self.currentHand)
                self.handFinished = True
            else:
                # Hand need to be finished, so indicate only who is
                # next player
                nextPlayer = playerNr - 1
                if nextPlayer == -1:
                    nextPlayer = 3
                self.nextPlayer = nextPlayer
                self.handFinished = False

        return (res, error)

    def checkMoon(self):

        try:
            i = self.gamePoints.index(26)

            # il giocatore i ha fatto shoot!
            for j in range (0, 4):
                if i == j:
                    self.totalPoints[j] += 0
                else:
                    self.totalPoints[j] += 26
        except:
            for j in range (0, 4):
                self.totalPoints[j] += self.gamePoints[j]

class Hand():
    """
    Represent an hand in hearts game
    """

    def __init__(self, brokenHearts):
        self.cards = []    # Cards in this hand, in order of play
        self.players = []  # Player in this hand, in order of play
        self.brokenHearts = brokenHearts

    def _playerHasInDeck(self, suit, deck):
        """
        Return true if deck contains 1+ cards of suit specified
        """
        assert(isinstance(suit, str))
        assert(isinstance(deck, list))

        for card in deck:
            assert(checkCardFormat(card))
            if card[0] == suit:
                return True

        return False

    def cardPlayed(self, player, playerDeck, card):
        """
        A card is played by a player, which has his playerDeck
        """
        assert(isinstance(player, int))
        assert(isinstance(playerDeck, list))
        assert(isinstance(card, str))
        assert(checkCardFormat(card))

        if len(self.players) > 0:
            # Get the first suit
            assert(len(self.cards) >= 1)
            suit = self.cards[0][0]

            # If the card hasn't the same suit, must check if is allowed
            p = re.compile(suit)
            if not p.match(card) and self._playerHasInDeck(suit, playerDeck):
                return (False, "You must play same suit of the first card")
        else:
            # This is the first play in the hand. Are the hearts broken?
            c = re.compile("C")
            if c.match(card) and not self.brokenHearts:
                has_only_h = True
                for card in playerDeck:
                    if not c.match(card):
                        has_only_h = False

                return (has_only_h, "Hearts aren't broken")

        self.cards.append(card)
        self.players.append(player)

        return (True, "none")

    def calcPoint(self):
        """
        Calculate points and winner of the hand.
        
        Return (int, int), which represent who win the hand and
        its points.
        """
        points = 0

        heartsPresent = re.compile("C")
        badQueen = re.compile("P10")

        for card in self.cards:
            if heartsPresent.match(card):
                points += 1
            if badQueen.match(card):
                points += 13

        whoWinNr = self._whoWin()
        return (whoWinNr, points)

    def _whoWin(self):
        """
        Return the player (as int) that win the hand
        """
        assert(len(self.cards) >= 1)

        suit = self.cards[0][0]

        max_num = 0
        player = -1

        i = 0
        while i < len(self.cards):
            card = self.cards[i]
            if suit == card[0]:
                # The suit is the same, I could calculate the max card number
                try:
                    num_card = int(card[1] + card[2])
                except:
                    # Card has only one digits as number
                    num_card = int(card[1])

                if num_card > max_num:
                    max_num = num_card
                    player = self.players[i]
            i += 1

        return player



class CardContainer(QLabel):
    """
    QLabel which contains a card as a pixmap
    """
    card = None     # The card

    cardClicked = QtCore.pyqtSignal(str) # Emitted when someone
                                         # click on the card (param: card name)

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)

    def mousePressEvent(self, event):
        QLabel.mousePressEvent(self, event)
        self.cardClicked.emit(self.card.name)

    def setPixmap(self, pixmap):
        if isinstance(pixmap, Card):
            self.card = pixmap
        QLabel.setPixmap(self, pixmap)

class Card(QPixmap):
    """
    A representation of card. It is a subclass of QPixmap, so it can
    be displayed on a CardContainer
    """
    def __init__(self, name="", parent=None):
        self.name = str(name)
        QPixmap.__init__(self, parent)
