# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Protocol, ClientFactory, ServerFactory, Factory
from PyQt4 import QtCore
import sys, time
from twisted.internet import reactor
from cards import *

class GameException(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return repr(self.msg)

class StatefulProtocol(LineReceiver):

    state = 'init'

    def lineReceived(self, string):
        """
        Choose a protocol phase function and call it.

        Call back to the appropriate protocol phase; this begins with
        the function proto_init and moves on to proto_* depending on
        what each proto_* function returns.  (For example, if
        self.proto_init returns 'foo', then self.proto_foo will be the
        next function called when a protocol message is received.
        """
        try:
            pto = 'proto_' + self.state
            statehandler = getattr(self, pto)
        except AttributeError:
            print 'callback' + self.state + 'not found'
        else:
            self.state = statehandler(string)
            if self.state == 'done' or self.state == "DONE":
                self.transport.loseConnection()

class CNetwork(StatefulProtocol, QtCore.QObject):
    """
    Network for an Hearts-Client.
    States:
      - init: Send our name (a number or OBS)
      - OBS:  We are observer
      - WMD:  Waiting My Deck
      - GR:   Game Ready
      - W3C:  Waiting three cards
      - GAME: normal game
      - WOK:  Waiting OK when we send a card
      - END:  game ended, score arrived
    """

    isObs = False

    def __init__(self):
        QtCore.QObject.__init__(self)

    def connectionMade(self):
        self.factory.server = self
        self.factory.connected.emit()
        StatefulProtocol.connectionMade(self)

    def connectionLost(self, reason=None):
        StatefulProtocol.connectionLost(self, reason=reason)
        self.factory.deconn.emit()

    def proto_init(self, string):
        """
        Initial state. The server send us our name (a number) or the
        string "OBS". If we are OBS, go directly to the GAME state, otherwise
        go to WMD state.
        """

        self.factory.clientName = string
        #print "My name: " + string

        if string == "OBS":
            self.isObs = True
            self.factory.iAmObs.emit()
            return "GAME"
        else:
            self.sendLine(self.factory.ircName)
            return "WMD"

    def proto_WMD(self, string):
        """
        We are waiting our deck, and we receive it when this function
        is invoked.
        
        Next state: GR
        """

        #print self.factory.clientName + "My card: " + string
        self.factory.deck = string.split(",")
        self.factory.deck.sort()

        return "GR"

    def proto_GR(self, string):
        """
        We are waiting the starting of the game. When this function is called,
        the game has been started by the server.
        
        Next state: W3C
        """

        self.factory.deckArrived.emit(str(string), int(self.factory.clientName), self.factory.deck)


        return "W3C"

    def proto_W3C(self, string):
        """
        We are waiting 3 cards from other peer. When this function is called, 
        in the parameter string we have our 3 cards.
        
        Next state: GAME or WOK
        """
        for card in string.split(","):
            self.factory.deck.append(card)
        self.factory.deck.sort()

        self.factory.cArrived.emit(self.factory.deck)

        if ("F00" in self.factory.deck):
            self.factory.sendCard("F00")
            return "WOK"

        return "GAME"

    def proto_GAME(self, string):
        """
        Normal game.
        Could receive:
         - PLAY: It is our turn
         - RESET: Hand finished, must polish table
         - END_GAME: Game finished
         - a string with a card and who played it.
        If we play, after sending a card we must receive
        the OK from server before go foward.
        """
        if string == "PLAY":
            if len(self.factory.deck) <= 0:
                self.sendLine("END_CARDS")
            else:
                self.factory.mustPlay.emit()
                return "WOK"
        elif string.startswith("RESET"):
            self.factory.resetTable.emit(int(string.split(":")[1]))
        elif string == "END_GAME":
            return "END_GAME"
        else:
            assert(len(string.split(":")) == 3)
            self.factory.cardPlayed.emit(int(string.split(":")[0]), string.split(":")[1],
                                         string.split(":")[2])

        return "GAME"

    def proto_WOK(self, string):
        """
        We played a card, and server tell us if the play is allowed.
        If it is, we return a GAME state, otherwise invoke the
        GAME state and ask for another card.
        """

        card, result, message = string.split(":")

        if not result == "OK":
            self.factory.cardNotValid.emit(message)
            return self.proto_GAME("PLAY")

        self.factory.deck.remove(card)
        return "GAME"

    def proto_END_GAME(self, string):
        self.factory.gameEnded.emit(string)

        return "ANOTHER_GAME"

    def proto_ANOTHER_GAME(self, string):
        if string == "DISCONNECT":
            self.factory.deconn.emit()
            return "DONE"
        elif string == "RESTART":
            self.factory.restart.emit()
            return self.proto_init(self.factory.clientName)

class BotNetwork(CNetwork):

    def proto_W3C(self, string):
        if string == "PLAY":
            return self.proto_GAME(string)
        else:
            return CNetwork.proto_W3C(self, string)

    def proto_GR(self, string):
        """
        We are waiting the starting of the game. When this function is called,
        the game has been started by the server.
        We are bot, so play random 3 cards
        Next state: W3C
        """
        if len(string.split(";")) > 0:
            random.shuffle(self.factory.deck)
            self.factory.sendThreeCards(self.factory.deck[0],
                                        self.factory.deck[1],
                                        self.factory.deck[2])
        return "W3C"


    def proto_GAME(self, string):
        """
        Normal game. We are bot, so we are interested only in 
        PLAY and END_GAME string..
        """

        if string == "PLAY":
            if len(self.factory.deck) <= 0:
                self.sendLine("END_CARDS")
                self.test = True
            else:
                random.shuffle(self.factory.deck)
                time.sleep(0.2)
                self.factory.sendCard(self.factory.deck[0])
                return "WOK"
        elif string == "END_GAME":
            return "END_GAME"

        return "GAME"

    def proto_END_GAME(self, string):
        return "ANOTHER_GAME"

class CFactory(ClientFactory, QtCore.QObject):
    protocol = CNetwork
    deck = []             # Client deck

    connected = QtCore.pyqtSignal()
    iAmObs = QtCore.pyqtSignal()
    deckArrived = QtCore.pyqtSignal(str, int, list)
    cArrived = QtCore.pyqtSignal(list)
    mustPlay = QtCore.pyqtSignal()
    cardNotValid = QtCore.pyqtSignal(str)
    cardPlayed = QtCore.pyqtSignal(int, str, str)
    resetTable = QtCore.pyqtSignal(int)
    gameEnded = QtCore.pyqtSignal(str)
    deconn = QtCore.pyqtSignal()
    restart = QtCore.pyqtSignal()

    def __init__(self, ircName, parent=None):
        super(QtCore.QObject, self).__init__()
        self.ircName = ircName

    def sendThreeCards(self, card1, card2, card3):
        """
        Send three cards to the server
        
        @param card1 First card
        @type card1 str
        
        @param card2 Second card
        @type card2 str
        
        @param card3 Third card
        @type card3 str

        """

        for card in (card1, card2, card3):
            try:
                self.deck.remove(card)
            except:
                print "Barone di merda non puoi mandare una carta che non hai"
                return

        self.server.sendLine(card1 + "," + card2 + "," + card3)

    def sendCard(self, card):
        """
        Send a single card
        
        @param card Card
        @type card str
        """
        if (card in self.deck):
            self.server.sendLine(self.clientName + ":" + self.ircName + ":" + card)
        else:
            print "Barone di merda non puoi mandare una carta che non hai"

class BotFactory(CFactory):
    protocol = BotNetwork

class SNetwork(StatefulProtocol, QtCore.QObject):
    """
    Network for an Hearts-Server.
    States:
      - init: The initial state, we exit from here when a client connects and send us
              an hello string
      - 3CA:  Three cards arrived from client
      - GAME: Normal game
    """
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.isObs = False
        self.ignoreClientRequest = (False, None)

    def connectionMade(self):
        """
        When a connection is made, add the client to the player pool only if we don't
        have 4 client. Otherwise, add it to the obs pool.
        """
        if (len(self.factory.clients) >= 4):
            self.isObs = True
            self.factory.obs.append(self)
            self.sendLine("OBS") # init state take OBS
        else:
            if len(self.factory.clientToReplace) > 0:
                idx, oldclient = self.factory.clientToReplace.popitem()
                self.ignoreClientRequest = (True, oldclient)

                newClientList = []
                moveNext = 0

                for i in (0, 1, 2, 3,):
                    if i != idx:
                        newClientList.append(self.factory.clients[i - moveNext])
                    else:
                        newClientList.append(self)
                        moveNext = 1

                self.factory.clients = newClientList
                self.factory.clientToName.pop(oldclient)
                self.sendLine(str(idx)) # mando il nome
            else:
                self.factory.clients.append(self)
                self.sendLine(str(len(self.factory.clients) - 1))

        StatefulProtocol.connectionMade(self)

    def connectionLost(self, reason):
        StatefulProtocol.connectionLost(self, reason)
        if self.isObs:
            self.factory.obs.remove(self)
        else:
            self.factory.replace(self)

    def proto_init(self, string):
        """
        Called when a client send us its name string,
        we send back its cards
        """

        self.factory.clientToName[self] = string

        if not self.isObs:
            if not self.ignoreClientRequest[0]:
                self.factory.net_giveCards(self)
                return "3CA"
            else:
                oldclient = self.ignoreClientRequest[1]
                try:
                    deck = self.factory.clientToDeck.pop(oldclient)
                    self.factory.clientToDeck[self] = deck
                    self.sendLine(",".join(deck)) # mando il deck
                except KeyError:
                    print "UFF"

                self.ignoreClientRequest = (False, None)

                if not self.factory.cardsPassed: # Non è stato fatto il passing
                    if not self.factory.clientTo3C.has_key(oldclient): # Se non ha mandato le carte
                        self.sendLine("GR")
                        return "3CA"
                    else: # Le aveva mandate
                        cards = self.factory.clientTo3C.pop(oldclient)
                        self.factory.clientTo3C[self] = card

                if oldclient.state == "GAME":
                    self.sendLine("NEXT")
                    if self.factory.gameLogic.nextPlayer == self.factory.clients.index(self):
                        self.sendLine("PLAY")

                return "GAME"


    def proto_3CA(self, string):
        """
        Called when three cards arrived from client
        """

        if not self.isObs:
            self.factory.net_threeCardsArrived(self, string)

        return "GAME"

    def proto_GAME(self, string):
        """
        Standard game
        """

        if string == "END_CARDS":
            if self.factory.net_noMoreCards():
                return "DONE"
            else:
                return "init"

        try:
            playerNr = int(string.split(":")[0])
            playerName = string.split(":")[1]
            cardPlayed = string.split(":")[2]
        except:
            self.sendLine("??:ERROR:Can't decode string")
            return "GAME"

        try:
            self.factory.net_cardArrived(playerNr, cardPlayed)
            self.sendLine(cardPlayed + ":OK:None")

            # update obs and client
            for obs in self.factory.obs:
                obs.sendLine(string)

            for client in self.factory.clients:
                client.sendLine(string)

            self.factory.net_inviteNextUserToPlay()

        except GameException as e:
            self.sendLine(cardPlayed + ":ERROR:" + e.msg)

        return "GAME"

class SFactory(ServerFactory, QtCore.QObject):

    protocol = SNetwork
    replaceSomeone = QtCore.pyqtSignal()

    def __init__(self, ircProtocol, gameName, parent=None):
        super(QtCore.QObject, self).__init__()
        self.ircProtocol = ircProtocol
        self.gameName = str(gameName)
        self.clients = []        # List of 4 clients, in order of connection
        self.obs = []            # List of obs, in order of connection
        self.clientToDeck = {}   # Map from client to their deck
        self.clientTo3C = {}     # Map from client to their 3 cards to pass
        self.clientToName = {}   # Map from client to its irc name
        self.clientToReplace = {}
        self.gamesPlayed = 0
        self.gameLogic = Game(self)
        self.gameLogic.brokenHeartsSign.connect(self._brokenHearts)
        self.completeDeck = makeRandomDeck()
        self.gamesPlayed = 0
        self.cardsPassed = False
        self.mustReplace = True

    def _brokenHearts(self):
        self.ircProtocol.msg(self.gameName, str("The Hearts are BROKEN!!!"))

    def replace(self, client):
        if not self.mustReplace:
            return

        self.ircProtocol.msg(self.gameName, str("The player " + self.clientToName[client] + " has disc."))
        idx = self.clients.index(client)
        self.clients.remove(client)
        self.clientToReplace[idx] = client
        self.replaceSomeone.emit()

    def discAll(self):
        for client in self.clients:
            client.transport.loseConnection()

        for ob in self.obs:
            ob.transport.loseConnection()

    def _giveCards(self, client):
        """
        Return a list of 13 cards from the deck. If no cards left, return an
        empty string
        """

        try:
            playerDeck = self.completeDeck[0:13]
        except:
            return  ""

        self.completeDeck = self.completeDeck[13:]
        self.clientToDeck[client] = playerDeck
        self.ircProtocol.msg(self.gameName, str("The player " + self.clientToName[client] + " has connected."))
        return playerDeck

    def net_giveCards(self, client):
        """
        Send 13 cards to client. If it is the 4th client, send a 
        "Game Ready" command to all clients
        
        @param client Client
        @type client CNetwork
        """

        deck = self._giveCards(client)
        client.sendLine(",".join(deck))

        if (len(self.completeDeck) == 0 and deck != ""):
            names = ""
            i = 0
            for client in self.clients:
                names += str(i) + ":" + self.clientToName[client] + ";"
                i = i + 1
            for client in self.clients:
                client.sendLine(names)

            self.completeDeck = makeRandomDeck()
            self.ircProtocol.msg(self.gameName, str("Let's the game init! Select three cards to pass"))

    def net_threeCardsArrived(self, client, cards):
        """
        A client send us its 3 cards. Store them first:
        then, if all clients have sent their cards, do the passing.
        
        @param client Client who sent
        @type client CNetwork
        
        @param cards Cards that the client have passed
        @type cards str, make a list with split(",")
        """

        if client not in self.clients:
            print "An obs spoofed him as client"
            return

        # Save the cards sent by client, and remove them from its deck
        self.clientTo3C[client] = cards

        deck = self.clientToDeck[client]
        for card in cards.split(","):
            try:
                deck.remove(card)
            except:
                print "Sto bastardo mi dice che ha delle carte che in realtà non gli ho mai spedito"

        # Do the passing
        if (len(self.clientTo3C) == 4):
            self._distribuiteCards()

    def _distribuiteCards(self):
        """
        Passing card algorithm.
        """

        whoSend = self.clients[0]

        for whoReceive in reversed(self.clients):
            # Send the cards over the network
            cardsToSend = self.clientTo3C[whoSend]
            whoReceive.sendLine(cardsToSend)

            # Update server's vision of client deck
            receiverDeck = self.clientToDeck[whoReceive]
            for card in cardsToSend.split(","):
                receiverDeck.append(card)

            whoSend = whoReceive

        self.clientTo3C.clear()
        self.cardsPassed = True

    def net_cardArrived(self, playerNr, cardPlayed):
        """
        Someone played a card. Let's find who, if he has the card he played,
        and update other client's and observer.
        """
        try:
            clientWhoPlayed = self.clients[playerNr]
            clientDeck = self.clientToDeck[clientWhoPlayed]
        except:
            raise GameException("You spoof. I need to ban you..")

        (res, error) = self.gameLogic.cardPlayed(playerNr, clientDeck, cardPlayed)

        if not res:
            raise GameException(error)
        else:
            clientDeck.remove(cardPlayed)

    def net_inviteNextUserToPlay(self):
        """
        Invite next user to play, or if the hand is finished before
        play clear client's table.
        """
        if self.gameLogic.handFinished:
            self.ircProtocol.msg(self.gameName, "The player " + self.clientToName[self.clients[self.gameLogic.nextPlayer]] + " has won the round.")
            for client in self.clients:
                client.sendLine("RESET:" + str(self.gameLogic.nextPlayer))
            for obs in self.obs:
                obs.sendLine("RESET:" + str(self.gameLogic.nextPlayer))
        self.clients[self.gameLogic.nextPlayer].sendLine("PLAY")

    def net_noMoreCards(self):
        """
        A client finished its cards.
        
        Return true if someone reached 100 points, otherwise return false
        """

        i = 0
        str_points = ""
        final_end = False

        self.gamesPlayed += 1

        if self.gamesPlayed >= 13:
            final_end = True

        self.gameLogic.checkMoon()

        for points in self.gameLogic.totalPoints:
            if points >= 100:
                final_end = True
            str_points += self.clientToName[self.clients[i]] + ":" + str(points) + ";"
            i += 1

        if final_end:
            self.mustReplace = False

        for client in self.clients:
            client.sendLine("END_GAME")
            client.sendLine(str_points)
            if final_end:
                client.sendLine("DISCONNECT")
                client.transport.loseConnection()
            else:
                client.sendLine("RESTART")
                client.state = "init"

        for obs in self.obs:
            obs.sendLine("END_GAME")
            obs.sendLine(str_points)
            if final_end:
                obs.sendLine("DISCONNECT")
                obs.transport.loseConnection()
            else:
                obs.sendLine("RESTART")
                obs.state = "init"


        self.gameLogic.resetGame()
        return final_end


if __name__ == "__main__":
    from twisted.internet.endpoints import TCP4ClientEndpoint

    type = "s"

    if len(sys.argv) > 1:
        type = sys.argv[1]

    if (type == "s"):
        print "Starting as a Server"
        factory = SFactory()
        reactor.listenTCP(6666, factory)
    else:
        print "Starting clients"
        for i in (0, 1, 2):
            point = TCP4ClientEndpoint(reactor, "localhost", 6666)
            point.connect(CFactory())

    reactor.run()
