# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from ui_game import Ui_Game
from ui_dialogpoint import Ui_DialogPoint

from network import *
from cards import Card

from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.internet import defer
import time

class dialogPoint(Ui_DialogPoint, QDialog):

    def __init__(self, points, parent=None):
        assert(isinstance(points, dict))
        QDialog.__init__(self, parent)
        self.setupUi(self)
        model = QStandardItemModel(self)
        if len(points) == 0:
            msg = QStandardItem(QString("No points arrived from server.. play an entire hand"))
            msg.setEditable(False)
            model.appendRow(msg)
        else:
            header = QStringList()
            header.append("User")
            header.append("Points")
            model.setHorizontalHeaderLabels(header)

        for k, v in points.iteritems():
            who = QStandardItem(QString(str(k)))
            point = QStandardItem(QString(str(v)))
            who.setEditable(False)
            point.setEditable(False)
            model.appendRow((who, point,))

        self.tablePoints.setModel(model)

class gameWindow(QGroupBox, Ui_Game):
    """
    The Game.
    """

    ProblemConnection = QtCore.pyqtSignal()
    End = QtCore.pyqtSignal(dict)
    Playing = QtCore.pyqtSignal()

    def __init__(self, ircNick, ircProtocol, parent=None):
        QGroupBox.__init__(self, parent)
        Ui_Game.setupUi(self, self)
        self.ircProtocol = ircProtocol
        self.points = {} # Points.
        self.ircNick = ircNick
        self._init()
        self.isObs = False
        self.isOwner = False
        self.isInGame = False

    def _init(self):
        state = "None"  # Variable that represent the state of the game
                        # from the user view.
                        # 3C == User is selecting 3 cards to pass
                        # WAIT == User is waiting starting of the game
                        # P == Play in progress
        self.cardsToPass = []
        self.mustMemHand = False   # True when user doesn't want to update the UI 
        self.cardPlayedInHand = {} # Where memorize hand card and player

    def createGame(self, name, botNumber, portNumber):
        """
        Create a game, starting a server and a client for each bot
        specified. Finally, join in the game.
        """
        self.isOwner = True
        self.ircProtocol.msg("#home", str("A game named " + name + " has been created :)"))
        self.setTitle(name)
        self.portNumber = int(portNumber)
        botNumber = int(botNumber)

        # Server
        self.Sfactory = SFactory(self.ircProtocol, "#" + str(name), self)
        self.Sfactory.replaceSomeone.connect(self._replace)
        self.portListening = reactor.listenTCP(portNumber, self.Sfactory)
        self.botConnector = []

        # Bot
        while botNumber > 0:
            self.botConnector.append(reactor.connectTCP("localhost", self.portNumber,
                                                        BotFactory("BOT" + str(botNumber))))
            botNumber = botNumber - 1

        self.joinGame("localhost", portNumber)

    def _replace(self):
        names = ["NAT", "CARLO", "MARTIN"]
        random.shuffle(names)
        random.shuffle(names)
        self.botConnector.append(reactor.connectTCP("localhost", self.portNumber,
                                                        BotFactory("BOT_" + names[0])))

    def joinGame(self, hostName, port):
        """
        Join on the game hosted by hostName, on the port specified.
        """

        self.factory = CFactory(self.ircNick)
        self.factory.deckArrived.connect(self._deckArrived)
        self.factory.cArrived.connect(self._cArrived)
        self.factory.mustPlay.connect(self._mustPlay)
        self.factory.cardNotValid.connect(self._cardNotValid)
        self.factory.cardPlayed.connect(self._cardPlayed)
        self.factory.resetTable.connect(self._resetTable)
        self.factory.iAmObs.connect(self._iAmObs)
        self.factory.gameEnded.connect(self._gameEnded)
        self.factory.deconn.connect(self._end)
        self.factory.restart.connect(self.nextHand)
        self.factory.connected.connect(self._connected)

        self.myConnector = reactor.connectTCP(str(hostName), int(port), self.factory)
        reactor.callLater(10, self._timeoutExplode)

    def _timeoutExplode(self):
        if self.isInGame:
            return

        msgBox = QMessageBox()
        msgBox.setText("Can't connect to the hosting game")
        msgBox.setInformativeText("Check your connection. Who host maybe have its port closed")
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.exec_()
        self.ProblemConnection.emit()

    def _connected(self):
        self.isInGame = True
        self.ircProtocol.join(str(self.title()))

    def _iAmObs(self):
        """
        The network inform us that we're obs
        """
        self.isObs = True

    def _deckArrived(self, players, ourNr, ourDeck):
        """
        The network give us our number and our deck. Display it on the table
        """
        assert(isinstance(ourNr, int))
        assert(isinstance(ourDeck, list))
        self.Playing.emit()

        self.imageDeck = {}
        self.playerNr = ourNr

        # Identify our place on the table
        frame = None
        if ourNr == 0:
            frame = self.topCard_2
        elif ourNr == 1:
            frame = self.leftCard_2
        elif ourNr == 2:
            frame = self.downCard_2
        elif ourNr == 3:
            frame = self.rightCard_2

        frame.setFrameShadow(QFrame.Raised)

        # Set names
        for player in players.split(";")[:-1]:
            nr = int(player.split(":")[0])
            name = str(player.split(":")[1])
            if nr == 0:
                self.labelTop.setText(name)
            elif nr == 1:
                self.labelLeft.setText(name)
            elif nr == 2:
                self.labelDown.setText(name)
            elif nr == 3:
                self.labelRight.setText(name)

        # Do our ui deck
        for card in ourDeck:
            label = CardContainer(self)
            imageCard = Card(card, label)
            label.cardClicked.connect(self._cardClicked)

            imageCard.load("media/" + card + ".gif")
            self.imageDeck[card] = label
            label.setPixmap(imageCard)
            self.allCards.addWidget(label)

        self.sendCardButton.setEnabled(True)
        self.state = "3C"

    def _cArrived(self, deck):
        """
        The network inform us that a new deck, with the 3 cards received
        from the initial passing, has arrived.
        """
        assert(isinstance(deck, list))
        # TODO: Sto rifacendo tutto da 0. Non sarebbe meglio fare un 
        # update del vecchio?
        for v in self.imageDeck.values():
            v.deleteLater()
        self.imageDeck.clear()

        for card in deck:
            label = CardContainer(self)
            imageCard = Card(card, label)
            label.cardClicked.connect(self._cardClicked)

            imageCard.load("media/" + card + ".gif")
            self.imageDeck[card] = label
            label.setPixmap(imageCard)
            self.allCards.addWidget(label)

        self.state = "P"

    def _cardClicked(self, card):
        """
        Someone clicked on a card, specified on the parameter card
        """
        card = str(card)
        assert(isinstance(card, str))
        assert(card in self.imageDeck)
        assert(self.state == "3C" or self.state == "P" or self.state == "WAIT")

        cardContainer = self.imageDeck[card]

        if self.state == "3C":
            if len(self.cardsToPass) <= 2 and str(card) not in self.cardsToPass:
                cardContainer.setGeometry(cardContainer.x(), cardContainer.y() - 10,
                                          cardContainer.width(), cardContainer.height())
                self.cardsToPass.append(str(card))
            elif str(card) in self.cardsToPass:
                cardContainer.setGeometry(cardContainer.x(), cardContainer.y() + 10,
                                      cardContainer.width(), cardContainer.height())
                self.cardsToPass.remove(str(card))
        elif self.state == "P":
            if len(self.cardsToPass) <= 0 and str(card) not in self.cardsToPass:
                cardContainer.setGeometry(cardContainer.x(), cardContainer.y() - 10,
                                          cardContainer.width(), cardContainer.height())
                self.cardsToPass.append(str(card))
            elif str(card) in self.cardsToPass:
                cardContainer.setGeometry(cardContainer.x(), cardContainer.y() + 10,
                                      cardContainer.width(), cardContainer.height())
                self.cardsToPass.remove(str(card))

    def _mustPlay(self):
        """
        The network inform us that is our turn
        """
        self.cardsToPass = []
        if not self.mustMemHand:
            self.sendCardButton.setEnabled(True)
        else:
            self.sblockSend = True


    def _cardNotValid(self, reason):
        """
        Card selected by the user isn't valid..
        """

        for card in self.cardsToPass:
            assert(card in self.imageDeck)
            cardContainer = self.imageDeck[card]
            cardContainer.setGeometry(cardContainer.x(), cardContainer.y() + 10,
                                      cardContainer.width(), cardContainer.height())
        msgBox = QMessageBox()
        msgBox.setText("Card not valid")
        msgBox.setInformativeText(reason)
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.exec_()

    def _cardPlayed(self, playerNr, playerName, card):
        """
        The player played a card.
        If the user don't want updates, simply memorize it. Else, we need
        to display (on the right place) the card played.
        """
        card = str(card)
        assert(isinstance(playerNr, int))

        if self.mustMemHand:
            self.cardPlayedInHand[playerNr] = (playerName, card,)
            return

        # If we are not obs and we play a card, remove it from the
        # deck displayed
        if not self.isObs and playerNr == self.playerNr:
            assert(card in self.imageDeck)
            cardContainer = self.imageDeck.pop(card)
            self.allCards.removeWidget(cardContainer)
            cardContainer.deleteLater()

        # Make a new visible card and place it on the table
        label = CardContainer(self)
        imageCard = Card(card, label)
        imageCard.load("media/" + card + ".gif")
        label.setPixmap(imageCard)

        assert(playerNr <= 3 and playerNr >= 0)
        if playerNr == 0:
            self.topCard.addWidget(label)
            self.labelTop.setText(playerName)
        elif playerNr == 1:
            self.leftCard.addWidget(label)
            self.labelLeft.setText(playerName)
        elif playerNr == 2:
            self.downCard.addWidget(label)
            self.labelDown.setText(playerName)
        elif playerNr == 3:
            self.rightCard.addWidget(label)
            self.labelRight.setText(playerName)

    def _resetTable(self, whoWin):
        """
        Hand finished. User can go for another hand if he press the button
        Next. Else, the hand is memorized and displayed after the pressing of 
        Next button.
        Obs can't do things, only observe..
        """
        self.mustMemHand = True
        self.state = "WAIT"

        if not self.isObs:
            self.nextButton.setEnabled(True)
            self.sendCardButton.setEnabled(False)
            if whoWin == 0:
                self.topCard_2.setLineWidth(3)
            elif whoWin == 1:
                self.leftCard_2.setLineWidth(3)
            elif whoWin == 2:
                self.downCard_2.setLineWidth(3)
            elif whoWin == 3:
                self.rightCard_2.setLineWidth(3)
        else:
            d = defer.Deferred()
            d.addCallback(self.nextHand)
            reactor.callLater(2, d.callback, None)

    def sendCards(self):
        """
        Someone press the send button. Check in what state we are and
        do the right action.
        """

        assert(self.state == "3C" or self.state == "P")

        if self.state == "3C" and len(self.cardsToPass) == 3:
            self.sendCardButton.setEnabled(False)
            self.factory.sendThreeCards(self.cardsToPass[0],
                                        self.cardsToPass[1], self.cardsToPass[2])
            self.state = "WAIT"
            for card in self.cardsToPass:
                assert(card in self.imageDeck)
                cardContainer = self.imageDeck.pop(card)
                self.allCards.removeWidget(cardContainer)
                cardContainer.deleteLater()
            self.cardsToPass = []
        elif self.state == "P" and len(self.cardsToPass) == 1:
            self.sendCardButton.setEnabled(False)
            self.factory.sendCard(self.cardsToPass[0])

    def nextHand(self, argIgnored=None):
        """
        Next button pressed.
        Delete the cards on the table, and update it with the new cards arrived.
        """
        for layout in (self.downCard, self.topCard, self.rightCard, self.leftCard):
            item = layout.itemAt(0)
            if not item:
                continue
            layout.removeItem(item)
            if item.widget():
                item.widget().deleteLater()
        for frame in (self.downCard_2, self.topCard_2, self.rightCard_2, self.leftCard_2):
            frame.setLineWidth(1)

        self.nextButton.setEnabled(False)
        try:
            if self.sblockSend:
                self.sendCardButton.setEnabled(True)
        except:
            pass
        self.mustMemHand = False
        self.state = "P"

        for k, v in self.cardPlayedInHand.iteritems():
            self._cardPlayed(k, v[0], v[1])
            time.sleep(0.5)

        self.cardPlayedInHand.clear()

    def clearAll(self):
        if self.isOwner:
            self.ircProtocol.msg("#home", str("A game called " + self.title() + " has been closed."))
            self.portListening.stopListening()
            for con in self.botConnector:
                con.disconnect()

            self.Sfactory.discAll()

        self.myConnector.disconnect()
        self.isInGame = False
        self.isOwner = False
        self.isObs = False

        self.sendCardButton.setEnabled(False)
        self.nextButton.setEnabled(False)

        for layout in (self.downCard, self.topCard, self.rightCard, self.leftCard):
            item = layout.itemAt(0)
            if not item:
                continue
            layout.removeItem(item)
            if item.widget():
                item.widget().deleteLater()

        try:
            for k, v in self.imageDeck.iteritems():
                self.allCards.removeWidget(v)
                v.deleteLater()

            self.imageDeck.clear()
        except:
            pass

    def _end(self):
        msgBox = QMessageBox()
        msgBox.setText("Game finished")
        msgBox.setInformativeText("Thank you for playing a game in Marnatarlo. Your stats will be updated soon.")
        msgBox.setIcon(QMessageBox.Information)
        msgBox.exec_()
        self.clearAll()
        self.End.emit(self.points)
        self.points = {}

    def _gameEnded(self, point):
        point = str(point)
        assert(point != "")
        try:
            users = point.split(";")
            users = users[0:4]
            for user in users:
                pp = user.split(":")
                self.points[pp[0]] = int(pp[1])
        except:
            print "Malformed points.. I received " + point

        dialog = dialogPoint(self.points, self)
        dialog.exec_()

        self._init()
