#!/usr/bin/env python2.7

import os

commands = ("pyuic4 -o ui_mainwindow.py ui_mainwindow.ui",
	"pyuic4 -o ui_dialogsettings.py ui_dialogsettings.ui",
	"pyuic4 -o ui_dialoglist.py ui_dialoglist.ui",
	"pyuic4 -o ui_dialogpoint.py ui_dialogpoint.ui",
	"pyuic4 -o ui_game.py ui_game.ui")

for command in commands:
    print command
    os.system(command)
