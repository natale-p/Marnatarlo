# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import sys, qt4reactor
app = QApplication(sys.argv)
qt4reactor.install()

from ui_mainwindow import Ui_MainWindow
from gamewindow import *

from settings import DialogSettings
from dialoglist import DialogList
from irccore import IRCCore, IRCCoreFactory
from twisted.internet import reactor, protocol
from twisted.internet.endpoints import TCP4ClientEndpoint

class MainWindow(QMainWindow, Ui_MainWindow):

    nickname = ""
    password = ""
    protocol = None
    actualChan = ""
    connected = False

    chatBoxes = {"default": ""}   # Map channel names -> content.
    userInRoom = {"default" : []} # Map channel names -> user present
    userStatus = {}               # Map user names -> status, played, win

    def __init__(self, parent=None):
        """
        Create a new instance: initialize the UI, set the app name and domain.
        Then, control if we have settings: if not, make a settings file with default
        values.
        """
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        QCoreApplication.setOrganizationName("MNC");
        QCoreApplication.setOrganizationDomain("mnc.com");
        QCoreApplication.setApplicationName("Marnatarlo");
        settings = QSettings()
        if (not settings.contains("version")):
            self.__initialize_settings()

        self.listChan.setModel(QStandardItemModel())
        self.listUser.setModel(QStandardItemModel())

    def __initialize_settings(self):
        """
        Initialize settings with default value
        """
        settings = QSettings()
        settings.setValue("version", "0.0.1")
        settings.setValue("ident/nick", "default")
        settings.setValue("ident/password", "default")
        settings.setValue("network/remote_server", "localhost")
        settings.setValue("network/remote_port", 8007)
        settings.setValue("network/local_server", "localhost")
        settings.setValue("network/local_port", 6666)
        self.showSettings()

    def showInfoDialog(self, msg, detailedMsg):
        msgBox = QMessageBox()
        msgBox.setText(msg)
        msgBox.setInformativeText(detailedMsg)
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.exec_()

    def showSettings(self):
        """
        Show the dialog for change/view settings
        """
        winSett = DialogSettings()
        winSett.exec_()

    def connect(self):
        """
        The connect action is pressed. Start a new TCP connection
        with the value taken from settings
        """
        if self.connected:
            self.showInfoDialog("Error", "You are already connected")
            return
        settings = QSettings()
        self.nickname = str(settings.value("ident/nick").toString())
        self.password = str(settings.value("ident/password").toString())
        server = settings.value("network/remote_server").toString()
        port = settings.value("network/remote_port").toInt()

        reactor.connectTCP(str(server), int(port[0]), IRCCoreFactory(self))
        reactor.run()

    def showPoints(self):
        if not self.connected:
            self.showInfoDialog("Error", "You aren't connected")
            return
        try:
            dialog = dialogPoint(self.gameWindow.points, self)
            dialog.exec_()
        except:
            self.showInfoDialog("Error", "You aren't in a game")

    def quitGame(self):
        if not self.connected:
            self.showInfoDialog("Error", "You aren't connected")
            return
        try:
            if not self.gameWindow.isInGame:
                self.showInfoDialog("Error", "You aren't in a game.")
                return
            self._endAllGame(None)
        except:
            self.showInfoDialog("Error", "No game window :(")

    def showListGames(self):
        if not self.connected:
            self.showInfoDialog("Error", "You aren't connected")
            return
        try:
            if self.gameWindow.isInGame:
                self.showInfoDialog("Error", "You are already in a game. Quit before joining")
                return
        except:
            pass
        self.protocol.sendLine("LIST")

    def showCreateGame(self):
        if not self.connected:
            self.showInfoDialog("Error", "You aren't connected")
            return
        try:
            if self.gameWindow.isInGame:
                self.showInfoDialog("Error", "You are already in a game. Quit before creating")
                return
        except:
            pass

        name, ok = QInputDialog.getText(self, "Please insert the value",
                                        "What's the room name?")

        if not ok:
            return

        bot, ok = QInputDialog.getInt(self, "Please insert the value",
                                  "How many bots you want?", 0, 0, 4, 1)
        if not ok:
            return

        settings = QSettings()
        port = int(settings.value("network/local_port").toInt()[0])
        addr = str(settings.value("network/local_server").toString())

        command = "MKGAME " + str(name) + " " + str(bot) + " " + addr + " " + str(port)
        self.protocol.sendLine(command)
        self._makeNewGameWindow()
        self.gameWindow.createGame(name, bot, port)

    def closeEvent(self, event):
        try:
            self.gameWindow.clearAll()
        except:
            print "No gamewindow to clear"

        reactor.sigInt("")
        event.accept()

    def send(self):
        """
        Send button is pressed. Control that we are connected, and send
        a message throught our protocol. The message is taken from
        the textLine field, and it is sent to the channel actually
        displayed on the GUI
        """
        if protocol:
            msg = self.textLine.text()
            if msg.startsWith("/"):
                self.protocol.sendLine(str(msg)[1:])
            else:
                self.protocol.msg(str(self.actualChan), str(msg.toLatin1()))
            self.textLine.setText("")
        else:
            print "not connected"

    def msgReceivedFromChan(self, user, channel, msg):
        """
        A message is received from user in the channel
        """
        if channel[0] != '#' and channel != "default":
            channel = '#' + channel
        try:
            self.chatBoxes[channel] += user + "> " + msg + "\n"
        except:
            self.chatBoxes["default"] += (msg or "") + "\n"

        self._updateGui()

    def delRoom(self, room):
        rootItem = self.listChan.model().invisibleRootItem().child(0, 0)
        i = rootItem.rowCount() - 1
        while i >= 0:
            item = rootItem.child(i, 0)
            if item.text() == room:
                rootItem.takeChild(i, 0)
                rootItem.removeRow(i)
                break
            i = i - 1

        del self.chatBoxes[room]

    def initNewRoom(self, room):
        """
        Init a new chat room in the GUI. It adds a widget on the left
        with the name of the room
        """
        model = self.listChan.model()
        item = QStandardItem(QString(room))
        item.setEditable(False)
        model.invisibleRootItem().child(0, 0).appendRow(item)
        self.chatBoxes[room] = ""

    def listOfChannelsArrived(self, list):
        if len(list) == 0:
            self.showInfoDialog("Error", "No games present on the server")
            return

        winList = DialogList(list)
        if winList.exec_():
            try:
                if self.gameWindow.isInGame:
                    self.showInfoDialog("Error", "You are already in a game. Quit before joining")
                    return
            except:
                self._makeNewGameWindow()

            self.gameWindow.joinGame(winList.addr, winList.port)
            self.gameWindow.setTitle(winList.chan)

    def _makeNewGameWindow(self):
        self.gameWindow = gameWindow(self.nickname, self.protocol, self)
        self.centralwidget.layout().addWidget(self.gameWindow, 0, 1, 1, 2)
        self.gameWindow.End.connect(self._endAllGame)
        self.gameWindow.ProblemConnection.connect(self._endAllGame)
        self.gameWindow.Playing.connect(self._playing)

    def _chanChanged(self, index):
        model = self.listChan.model()
        if index is not None:
            self.actualChan = str(model.itemFromIndex(index).text())
        if self.actualChan != "Marnatarlo server":
            self.sendButton.setEnabled(True)
        else:
            self.sendButton.setEnabled(False)

        self._updateGui()

    def _updateGui(self):
        """
        An user clicked on a channel, we must update
        the GUI with the contents of the channel itself
        """

        model = self.listUser.model()
        model.clear()
        try:
            for user in self.userInRoom[self.actualChan]:
                status, played, won = self.userStatus[user]
                icon = None
                points = str((int(played) - int(won)) * -1 + int(won) * 4)

                if status == "NONE":
                    icon = QStandardItem(QIcon("media/green.png"), user + " " + points)
                elif status == "WAITING":
                    icon = QStandardItem(QIcon("media/yellow.png"), user + " " + points)
                elif status == "PLAYING":
                    icon = QStandardItem(QIcon("media/red.png"), user + " " + points)
                icon.setEditable(False)
                model.appendRow(icon)
        except:
            pass

        try:
            self.textIrc.setPlainText(self.chatBoxes[self.actualChan])
        except:
            self.textIrc.setPlainText(self.chatBoxes["default"])
        c = self.textIrc.textCursor();
        c.movePosition(QTextCursor.End);
        self.textIrc.setTextCursor(c)

    def _playing(self):
        self.protocol.sendLine("PLAY")

    def _endAllGame(self, points=None):

        self.gameWindow.clearAll()
        room = str (self.gameWindow.title())
        self.gameWindow.deleteLater()
        del self.gameWindow
        if points is None:
            self.partChan(room, ":(")
        else:
            min = 200
            whoWin = ""
            for user, point in points.iteritems():
                if point < min:
                    min = point
                    whoWin = user

            self.partChan(room, whoWin)

    def joinChan(self, chan):
        self.protocol.join(str(chan))

    def partChan(self, chan, winner):
        self.protocol.leave(str(chan), winner)
