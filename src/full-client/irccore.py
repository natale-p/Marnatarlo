# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
import string

class IRCCore(irc.IRCClient):
    nickname = ""
    password = ""
    listOfChannels = []

    def connectionMade(self):
        """
        Called when a connection is made.

        This may be considered the initializer of the protocol, 
        because it is called when the connection is completed. 
        For clients, this is called once the connection to the server
        has been established.
        """
        self.nickname = self.factory.window.nickname
        self.password = self.factory.window.password
        irc.IRCClient.connectionMade(self)
        self.factory.window.protocol = self
        self.factory.window.connected = True

        model = self.factory.window.listChan.model()
        item = QStandardItem(QString("Marnatarlo server"))
        model.invisibleRootItem().appendRow(item)

    def connectionLost(self, reason=None):
        irc.IRCClient.connectionLost(self, reason)
        self.factory.window.connected = False

    def myInfo(self, servername, version, umodes, cmodes):
        """
        Called with information about the server, usually at logon.
        """
        self.factory.window.msgReceivedFromChan("", "default", servername)
        self.factory.window.msgReceivedFromChan("", "default", version)
        self.factory.window.msgReceivedFromChan("", "default", umodes)
        self.factory.window.msgReceivedFromChan("", "default", cmodes)

    def created(self, when):
        """
        Called with creation date information about the server, usually at logon.
        """
        self.factory.window.msgReceivedFromChan("", "default", when)

    def userJoined(self, user, channel):
        self.factory.window.userInRoom[channel].append(user)
        self.sendLine("USERGAMEINFO :" + user)

    def userLeft(self, user, channel):
        self.factory.window.userInRoom[channel].remove(user)
        self.factory.window._updateGui()

    def connectionLost(self, reason):
        """
        Called when the connection is shut down.
        """
        model = self.factory.window.listChan.model()
        model.invisibleRootItem().removeRow(0)
        self.factory.window.msgReceivedFromChan("", "default", str(reason))
        self.factory.window.connected = False

    def joined(self, channel):
        """
        Called when I finish joining a channel.

        channel has the starting character ('#', '&', '!', or '+') intact.
        """

        self.factory.window.initNewRoom(channel)

    def left(self, channel):
        """
        Called when I have left a channel.

        channel has the starting character (C{'#'}, C{'&'}, C{'!'}, or C{'+'})
        intact.
        """
        self.factory.window.delRoom(channel)

    def privmsg(self, user, channel, msg):
        """
        Called when I have a message from a user to me or a channel.
        """
        if channel != "default":
            self.factory.window.msgReceivedFromChan(string.split(user, '!')[0], channel, msg)

    def msg(self, channel, message, length=irc.MAX_COMMAND_LENGTH):
        irc.IRCClient.msg(self, channel, message, length)
        self.factory.window.msgReceivedFromChan(self.nickname, channel, message)


    def signedOn(self):
        """
        Called when we have succesfully signed on to server.
        """

        self.join("home")

    def irc_RPL_NAMREPLY(self, prefix, params):
        """
        params: [ user, '=', channel, user_list ]
        ['jon', '=', '#home', 'dan jon']
        """
        self.factory.window.userInRoom[params[2]] = []

        for user in params[3].split(' '):
            self.factory.window.userInRoom[params[2]].append(user)
            self.sendLine("USERGAMEINFO :" + user)

    def irc_RPL_LIST(self, prefix, params):
        if (params[1] == "home"):
            return
        chan = []
        chan.append(params[1]) # chan name
        chan.append(params[2]) # count of user
        chan.append(params[3]) # count of bot
        chan.append(params[4]) # addr
        chan.append(params[5]) # port

        self.listOfChannels.append(chan)

    def irc_RPL_LISTEND(self, prefix, params):
        self.factory.window.listOfChannelsArrived(self.listOfChannels)
        self.listOfChannels = []

    def irc_RPL_USERGAMEINFO(self, prefix, params):
        self.factory.window.userStatus[params[1]] = params[2:]
        self.factory.window._updateGui()

    def irc_ERR_PASSWDMISMATCH(self, prefix, params):
        """
        The password isn't correct
        """
        self.factory.window.showInfoDialog("Can't connect to the server",
                                           "User/Password mismatch. Register on the web service")
        self.factory.window.connected = False

    def irc_ERR_NICKNAMEINUSE(self, prefix, params):
        """
        User is already logged in
        """
        self.factory.window.showInfoDialog("Can't connect to the server",
                                           "User already connected")
        self.factory.window.connected = False

class IRCCoreFactory(protocol.ClientFactory):
    protocol = IRCCore
    def __init__(self, window):
        self.window = window
    def clientConnectionFailed(self, connector, reason):
        print('connection failed! :( ', reason.__str__())
        reactor.stop()
