# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ui_dialoglist import Ui_DialogList

class DialogList(QDialog, Ui_DialogList):
    chan = ""
    def __init__(self, list):
        QDialog.__init__(self)
        self.setupUi(self)
        model = QStandardItemModel(self)
        j = 0
        for i in list:
            assert(len(i) == 5)
            name = QStandardItem(i[0])

            if name.text() == "home" or name.text() == "#home":
                continue

            users = QStandardItem(i[1])
            bot = QStandardItem(i[2])
            addr = QStandardItem(i[3])
            port = QStandardItem(i[4])

            for item in name, users, bot, addr, port:
                item.setEditable(False)

            model.setItem(j, 0, name)
            model.setItem(j, 1, users)
            model.setItem(j, 2, bot)
            model.setItem(j, 3, addr)
            model.setItem(j, 4, port)
            j = j + 1

        header = QStringList()
        for i in ("Name", "User", "Bot", "Address", "Port"):
            header.append(i)
        model.setHorizontalHeaderLabels(header)
        self.channelView.setModel(model)

    def accept(self):
        try:
            model = self.channelView.model()
            indexes = self.channelView.selectedIndexes()
            self.chan = model.itemFromIndex(indexes[0]).text()
            self.addr = model.itemFromIndex(indexes[3]).text()
            self.port = model.itemFromIndex(indexes[4]).text()
            self.done(1)
        except:
            msg = QMessageBox()
            msg.setText("You haven't selected nothing")
            msg.exec_()
            self.done(0)

    def reject(self):
        self.done(0)
