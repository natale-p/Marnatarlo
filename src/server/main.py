# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

from webservice import *
from ircservice import *

if __name__ == "__main__":
    root = Resource()
    db = MDatabase("sql.db")
    root.putChild("registration", RegistrationPage(db))
    root.putChild("who", WhoPage(db))
    root.putChild("", homePage())
    root.putChild("css", File("css"))
    root.putChild("js", File("js"))
    root.putChild("images", File("images"))
    factory = server.Site(root)
    reactor.listenTCP(8080, factory)
    srv = IRCServer(db)
    reactor.listenTCP(8079, srv.getFactory())

    reactor.run()
