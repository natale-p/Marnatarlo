# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

# These are web-pages for our web-interface.

from twisted.web import server
from twisted.web.resource import Resource
from twisted.web.static import File
from twisted.internet import reactor
from twisted.enterprise import adbapi
from database import MDatabase
import cgi

def getHead():
    """
    Get the head for all our pages.
  
    TODO: Occorre farle un pò carine, con js e css. Questa funzione
    ritornerà l'head per tutte le pagine.
    """
    return """
<!doctype html>  
<head>
<meta charset="UTF-8">
<title>%s</title>
<link rel="icon" href="images/favicon.gif" type="image/x-icon"/>
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

<link rel="shortcut icon" href="images/favicon.gif" type="image/x-icon"/> 
<link rel="stylesheet" type="text/css" href="css/styles.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="js/slides.min.jquery.js"></script>
    </head>
    <body>
        <!--start container-->
    <div id="container">

    <!--start header-->

    <header>
 
    <!--start logo-->
    <a href="/" id="logo"><img src="images/logo.png" width="221" height="130" alt="logo"/></a>    

    <!--end logo-->
    
   <!--start menu-->

    <nav>
    <ul>
    <li><a href="/">01. Home</a></li>
    <li><a href="/registration">02. Registrazione</a></li>
    <li><a href="/who">03. Iscritti</a></li>

    </ul>
    </nav>
    <!--end menu-->
    

    <!--end header-->
    </header>
"""

def getFooter():
    """
    Get the footer for all our page.
    """
    return """
   <footer>
   <div id="FooterTwo">
   © 2011 Martin Nat Carlo
   </div>
    
   </footer>
   <!--end footer-->

   </div>
   <!--end container-->
   
   </body></html>
"""

class homePage(Resource):
    """
    The homepage!
    """
    def render_GET(self, request):
        page = (str(getHead()) % "Marnatarlo: an omni-comprensive Hearts game")
        page += """
        <script>
        $(function(){
            $('#slides').slides({
                preload: true,
                preloadImage: 'images/loading.gif',
                play: 5000,
                pause: 2500,
                hoverPause: true
            });
        });
        </script>

   <!--start intro-->

   <section id="intro">
   <div id="slides">
   <div class="slides_container">
   <img src="images/banner1.jpg" width="960" height="206" alt="banner">
   <img src="images/banner2.jpg" width="960" height="206" alt="banner">
   <img src="images/banner3.jpg" width="960" height="206" alt="banner">
   </div>
   
   </div>
 
   </section>
   <!--end intro-->
   
   
   <!--start holder-->

   <div class="holder_content">

   <section class="group1">
   <h1>Marnatarlo: Presentazione</h1>
   
   <article>
   
      <p>MarNatarlO è un’applicazione distribuita che implementa il famoso gioco di carte Hearts per 4
      giocatori. E’ stata sviluppata come progetto per il corso di Architetture Software dell’Università di
      Bologna, all’interno del CdL Magistrale in Informatica. Di conseguenza, particolare enfasi è stata
      posta sul design e la progettazione dell’architettura del sistema.</p>

      <p>In generale, il funzionamento di rete dell’applicazione risponde al modello Client – Server. Le
      caratteristiche salienti di cui l’utente può godere sono:</p>

   <ul>
   <li>Iscrizione al servizio via web</li>
   <li>Creazione di stanze di gioco</li>
   <li>Partecipazione a stanze di gioco</li>
   <li>Gioco contro amici e/o giocatori automatizzati</li>
   <li>Servizio di chat con ogni giocatore connesso e con i propri avversari durante le partite</li>
   <li>Consultazione online dei punteggi di ogni giocatore</li>
   <li>Classifica aggiornata in tempo reale</li>
   </ul>
   
   </article>

   </section>

   <aside>
   <h1>Tecnologie utilizzate</h1>
   <div class="content_menu">
   <ul>
   <li><a href="http://www.python.org/">Python</a></li>
   <li><a href="http://twistedmatrix.com">Twisted</a></li>
   <li><a href="http://www.riverbankcomputing.co.uk/software/pyqt/intro">PyQt4</a></li>
   </ul></div>
   <a class="photo_hover3" href="#"><img src="images/picture1.jpg" width="215" height="137" alt="picture1"/></a>
   <a href="css/MarNatarlO.pdf"><span class="button">Doc</span></a>   
   </aside>
   
  </div>
  <!--end holder-->
   """
        page += getFooter();
        return page


class WhoPage(Resource):
    """
    The registration page
    """

    def __init__(self, database):
         self.database = database

    def usersPres(self, users, request):
        htmlRender = """"""
        if len(users) > 0:
            htmlRender = """<h1>Attualmente sono registrati</h1>"""
        else:
            htmlRender = """<h1>Nessun utente registrato..Sii tu il primo!</h1>"""

        htmlRender += """<article id="users"><div class="table">"""
        htmlRender += """<div class="row"><div class="cell">Nome</div><div class="cell">Giocate</div><div class="cell">Vinte</div><div class="cell">Punti</div></div>"""
        for user in users:
            htmlRender += """<div class="row">"""
            htmlRender += """<div class="cell">%s</div>""" % user[0]
            htmlRender += """<div class="cell">%s</div>""" % user[1]
            htmlRender += """<div class="cell">%s</div>""" % user[2]
            htmlRender += """<div class="cell">%s</div></div>""" % str((int(user[1]) - int(user[2])) * -1 + int(user[2]) * 4)
        htmlRender += """</div></article>"""

        self.final(htmlRender, request)

    def dbError(self, dontknow, request):
        self.final("""<h1>Errore nel db tentando di selezionare gli utenti.</h1>""", request)

    def final(self, htmlUsersExisting, request):
        page = getHead() % "Marnatarlo: Chi è iscritto"
        page += """<section class="group1">"""
        page += str(htmlUsersExisting)
        page += """</section>"""

        page += getFooter()
        request.setHeader('content-length', str(len(page)))
        request.write(page)
        request.finish()

    def render_GET(self, request):
        content_type, encoding = 'text/html', 'UTF-8'
        request.setHeader('Content-Type', '%s; charset=%s' %
                          tuple(map(str, (content_type, encoding))))
        self.database.get_all_users().addCallback(self.usersPres, request);

        return server.NOT_DONE_YET

class RegistrationPage(Resource):

    def __init__(self, database):
         self.database = database

    def render_GET(self, request):
        page = getHead() % "Marnatarlo: Registrazione nuovo utente"
        page += """
           <section class="group1">
   <h1>Informazioni sulla registrazione</h1>
   
   <article>
    <p>Registrarsi a Marnatarlo è semplice. Inserisci l'username e la password
    che desideri, verrai registrato e potrai accedere al server chat.</p>
   </article>

   </section>
   <aside>
   <h1>Registrazione</h1>
            <form method=\"POST\">
            <input name=\"nick\" type=\"text\" />
            <input name=\"password\" type=\"password\" />
            <input type=\"submit\" value=\"Registrati!\" />
          </form>
   </aside>
   """ + getFooter()

        return page

    def complete(self, result, request):
        page = getHead() % "Marnatarlo: Registrazione nuovo utente"
        page += """
           <section class="group1">
   <h1>Registrazione accodata</h1>
   
   <article>
    <p>Prova a collegarti al client Marnatarlo con username e password che ci hai fornito.</p>
   </article>

   </section>
   """ + getFooter()
        request.setHeader('content-length', str(len(page)))
        request.write(page)
        request.finish()

    def render_POST(self, request):
        self.database.save_user(cgi.escape(request.args["nick"][0]), cgi.escape(request.args["password"][0]),).addCallback(self.complete, request)
        return server.NOT_DONE_YET


if __name__ == "__main__":
    root = Resource()
    db = MDatabase("sql.db")
    root.putChild("registration", RegistrationPage(db))
    root.putChild("who", WhoPage(db))
    root.putChild("", homePage())
    root.putChild("css", File("css"))
    root.putChild("js", File("js"))
    root.putChild("images", File("images"))
    factory = server.Site(root)
    reactor.listenTCP(8880, factory)
    reactor.run()
