# -*- coding: utf-8 -*-
#
# (c) 2011 Natale Patriciello <natale.patriciello@gmail.com>
#
# This file is part of Marnatarlo.
#
# Marnatarlo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Marnatarlo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Marnatarlo.  If not, see <http://www.gnu.org/licenses/>.
#

# This is the main IRC service for the server.
# It takes (under the IRC protocol) users and their password,
# deny login if user isn't registered via web interface.
#
# NOTE: group == chat room == game created
# The main chat room is "home" and is owned by "admin"

from twisted.words import service, ewords
from twisted.cred import checkers, credentials, portal, strcred, error
from twisted.internet import reactor, defer
from twisted.words.protocols import irc
from twisted.python import failure
from zope.interface import implements
from database import MDatabase

NICKSERV = 'NickServ!NickServ@services'

class MUser(service.User):

    def __init__(self, name, db):
        self.state = "NONE"
        self.db = db
        service.User.__init__(self, name)

    def join(self, group):
        if group.name != "home":
            self.state = "WAITING"
        return service.User.join(self, group)

    def leave(self, group, reason=None):
        if self.state == "PLAYING":
            if reason is not None and reason == "NOTIDISC":
                pass
            else:
                self.db.user_play(self.name)
            self.state = "NONE"
        if group.name != "home":
            self.state = "NONE"

        return service.User.leave(self, group, reason)

    def play(self):
        self.state = "PLAYING"

class MGroup(service.Group):
    """
    Single chat-room.
    Every chat-room has an owner (who create the room) and, if he/she
    part or quit, the room will be destroyed.
    """

    def __init__(self, db, name, ownerName, botNr, ownerAddr, ownerPort, realm):
        """
        Init the chatroom
        """

        assert(isinstance(botNr, int))
        self.ownerName = ownerName
        self.ownerAddr = ownerAddr
        self.ownerPort = ownerPort
        self.db = db
        self.botNr = botNr
        self.realm = realm
        service.Group.__init__(self, name)

    def remove(self, userLeaving, reason=None):
        """
        Someone ask to remove user from group. We must check that user isn't the
        owner: otherwise we must disconnect other partecipant from the room
        (sending a PART command to the server) and delete the room.
    
        @type userLeaving Something that implements IUser interface
        @param userLeaving User who want to leave this room
    
        @type reason C{str}
        @param reason Reason for leaving
        """

        d = defer.Deferred()

        if userLeaving.name == self.ownerName and userLeaving.name != "admin":
            if reason is None:
                print "The client named " + userLeaving.name + " has disc without givin winner name"
            winner = reason
            for user in self.users.values():

                if winner is not None and user.name == winner:
                    self.db.user_won(user.name)

                # REMOVING PART
                if user.name == self.ownerName:
                    # Normal removing here, he/she is the owner who asked to leave
                    d = service.Group.remove(self, user, reason)
                else:
	                # Send an irc PART command to the server
                    user.irc_PART("", (self.name, "NOTIDISC",))
            d.addCallback(self.realm.closeGroup, self)
        else:
            return service.Group.remove(self, userLeaving, reason)

        return d


class IRCMUser(service.IRCUser):
    """
    Protocol that represent an user in the server.
    This class take all server-side action in reply to
    user's commands.
  
    All these methods are called from low-level protocol.
  
    Every callback for user's commands is made in this way:
      def irc_USERCOMMAND(self, prefix, params)
    where USERCOMMAND is the command (like PART, JOIN..) and
    params are the parameters to the command itself.
    """

    def connectionMade(self):
        service.IRCUser.connectionMade(self)
        self.factory.clients.append(self)

    def connectionLost(self, reason=None):
        service.IRCUser.connectionLost(self, reason)
        self.factory.clients.remove(self)

    def irc_LIST(self, prefix, params):
        """
        List query
        """
        if params:
            # Return information about indicated channels
            try:
                channels = params[0].decode(self.encoding).split(',')
            except UnicodeDecodeError:
                self.sendMessage(
                    irc.ERR_NOSUCHCHANNEL, params[0],
                    ":No such channel (could not decode your unicode!)")
                return

            groups = []
            for ch in channels:
                if ch.startswith('#'):
                    ch = ch[1:]
                groups.append(self.realm.lookupGroup(ch))

            groups = defer.DeferredList(groups, consumeErrors=True)
            groups.addCallback(lambda gs: [r for (s, r) in gs if s])
        else:
            # Return information about all channels
            groups = self.realm.itergroups()

        def cbGroups(groups):
            def gotSize(size, group):
                return group.name, size, group.botNr, group.ownerAddr, group.ownerPort
            d = defer.DeferredList([
                group.size().addCallback(gotSize, group) for group in groups])
            d.addCallback(lambda results: self.list([r for (s, r) in results if s]))
            return d
        groups.addCallback(cbGroups)

    def list(self, channels):
        """Send a group of LIST response lines

        @type channel: C{list} of C{(str, int, int, str, int)}
        @param channel: Information about the channels being sent:
        their name, the number of participants, the number of bots, 
        owner address, owner port.
        """
        for (name, size, botNr, address, port) in channels:
            self.sendMessage(irc.RPL_LIST, name, str(size) + " " + str(botNr) +
                             " " + address + " " + port)
        self.sendMessage(irc.RPL_LISTEND, ":End of /LIST")

    def irc_MKGAME(self, prefix, params):
        """ 
        My client-side inform me that he has created a game.
        Commands is:
            /mkgame [nome_stanza] [numero_bot] [addr] [porta]
        """
        if len(params) < 4:
            self.sendMessage(irc.ERR_UNKNOWNCOMMAND,
                             " ", ":Can't decode the command. " +
                             "Must be /mkgame [name] [bot_number] [addr] [port]")
        elif self.avatar is None:
            self.sendMessage(irc.ERR_NOTREGISTERED,
                             " ", ":You aren't autenticated. No game creation permission.")
        else:
            self.realm.createGroup(unicode(params[0]), self.name, int(params[1]), params[2], params[3])

    def irc_JOIN(self, prefix, params):
        service.IRCUser.irc_JOIN(self, prefix, params)
        self.factory.updateUserStatus(self.name)

    def irc_PART(self, prefix, params):
        service.IRCUser.irc_PART(self, prefix, params)
        self.factory.updateUserStatus(self.name)

    def irc_USERGAMEINFO(self, prefix, params):
        """
        Asked gaming information about an user
        """

        if len(params) != 1:
            self.sendMessage(irc.ERR_UNKNOWNCOMMAND,
                             " ", ":Can't decode the command. " +
                             "Must be /usergameinfo [user_name]")
        elif self.avatar is None:
            self.sendMessage(irc.ERR_NOTREGISTERED,
                             " ", ":You aren't autenticated. No game creation permission.")
        else:
            def ebUser(err):
                self.sendMessage(
                    irc.ERR_NOSUCHNICK,
                    ":No such nickname.")
            def cbUser(user, cbError):
                def cbUserInfo(info):
                    self.sendMessage("RPL_USERGAMEINFO", params[0] + " " +
                                     user.state + " " + str(info[0][1]) +
                                     " " + str(info[0][2]))
                self.realm.getUserInfo(params[0]).addCallback(cbUserInfo)

            self.realm.lookupUser(unicode(params[0])).addCallback(cbUser, ebUser)

    def irc_PLAY(self, prefix, params):
        def cbUser(user):
            user.play()
            self.factory.updateUserStatus(self.name)

        self.realm.lookupUser(unicode(self.name)).addCallback(cbUser)

    def _ebLogin(self, err, nickname):
        """
        Called in case that login fail.
        """
        if err.check(ewords.AlreadyLoggedIn):
            self.privmsg(
                NICKSERV,
                nickname,
                "Already logged in.  No pod people allowed!")
            self.sendMessage("ERR_NICKNAMEINUSE")
        elif err.check(error.UnauthorizedLogin):
            self.privmsg(
                NICKSERV,
                nickname,
                "Login failed.  Goodbye.")
            self.sendMessage("ERR_PASSWDMISMATCH")
        else:
            print err
            self.privmsg(
                NICKSERV,
                nickname,
                "Server error during login.  Sorry.")
        self.transport.loseConnection()


class InMemoryMarRealm(service.InMemoryWordsRealm):
    """
    Non ho ben capito cosa sia, cmq credo sia il pezzo che crea (lato server)
    utenti/gruppi su richiesta dei client.
    L'ho fatta solo per poter creare gruppi con l'owner
    """

    def __init__(self, database, name):
        service.InMemoryWordsRealm.__init__(self, name)
        self.database = database

    def getUserInfo(self, user):
        return self.database.get_stats(user)

    def closeGroup(self, result, group):
        """
        Remove the chat room from the server
    
        @type group MGroup
        @param group The group to remove
        """

        del self.groups[group.name]

    def groupFactory(self, name, ownerName, botNr, ownerAddr, ownerPort):
        """
        Factory for MGroup (Group which have an owner)
    
        @type name unicode string
        @param name Name of the group
    
        @type owner C{str}
        @param owner Owner of the group
        """
        return MGroup(self.database, name, ownerName, botNr,
                      ownerAddr, ownerPort, self)

    def userFactory(self, name):
        return MUser(name, self.database)

    def createGroup(self, name, ownerName, botNr, ownerAddr, ownerPort):
        """
        Create group with owner (CTRL-C/CTRL-V from service.InMemoryWordsRealm,
        only added the owner for the call to groupFactory)
        """
        assert isinstance(name, unicode)
        def cbLookup(group):
            return failure.Failure(ewords.DuplicateGroup(name))
        def ebLookup(err):
            err.trap(ewords.NoSuchGroup)
            return self.groupFactory(name, ownerName, botNr, ownerAddr, ownerPort)

        name = name.lower()
        d = self.lookupGroup(name)
        d.addCallbacks(cbLookup, ebLookup)
        d.addCallback(self.addGroup)
        return d

class SQLitePasswordDB:
    """
      Implements an ICredentialsChecker, controlling user/pass
      on a SQLite database, via an object that return a user/password
      list by the get_user_login_info(username) method.
    """
    implements(checkers.ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword,
                            credentials.IUsernameHashedPassword)

    def __init__(self, database):
        """
        Init the checkers
    
        @type database is a MDatabase object
        @param database Our database
        """
        self.database = database

    def _cbPasswordMatch(self, matched, username):
        if matched:
            return username
        else:
            return failure.Failure(error.UnauthorizedLogin())

    def _cbGotUser(self, users, credentials):
        if len(users) <= 0:
            return failure.Failure(error.UnauthorizedLogin())
        return defer.maybeDeferred(
                                    credentials.checkPassword, users[0][1]).addCallback(
                                    self._cbPasswordMatch, str(credentials.username))

    def requestAvatarId(self, credentials):
        return self.database.get_user_login_info(credentials.username).addCallback(self._cbGotUser, credentials)

class IRCMFactory(service.IRCFactory):

    protocol = IRCMUser
    clients = []

    def buildProtocol(self, addr):
        p = service.IRCFactory.buildProtocol(self, addr)
        p.addr = addr
        return p

    def updateUserStatus(self, userName):
        print "Update " + userName + " status for all client " + str(len(self.clients))
        for client in self.clients:
            client.irc_USERGAMEINFO("", (userName,))

class IRCServer():
    """
      The IRC server.
      Design principles:
        *) Users must register via web to join the server
        *) Login information from SQLite db
        *) "home" channel is available everytime
        *) Other channels are created by users for game-chat and deleted when
           their owner left
    """
    def __init__(self, database):
        self.credCheckers = SQLitePasswordDB(database)
        self.wordsRealm = InMemoryMarRealm(database, "marnatarlo.com")
        self.wordsPortal = portal.Portal(self.wordsRealm, [self.credCheckers])
        self.wordsRealm.createGroup(u"home", "admin", 0, "", "")
        self.factory = IRCMFactory(self.wordsRealm, self.wordsPortal)

    def getFactory(self):
        return self.factory

if __name__ == "__main__":
    srv = IRCServer(MDatabase("sql.db"))
    reactor.listenTCP(8007, srv.getFactory())
    reactor.run()
